<?php

namespace Drupal\udon\Commands;

use Drupal\Core\KeyValueStore\KeyValueFactory;
use Drupal\Core\KeyValueStore\KeyValueMemoryFactory;
use Drush\Commands\DrushCommands;

class UdonCommands extends DrushCommands {

  /**
   * Run a single update.
   *
   * @command udon
   * @param $module
   *   The name of the module.
   * @param $number
   *   The update function number.
   */
  public function udon($module, $number) {
    $batch['operations'] = [
      [[static::class, 'run'], [$module, $number]],
      ['drupal_flush_all_caches', []],
    ];
    batch_set($batch);
    drush_backend_batch_process();
  }

  public static function run($module, $number, &$context) {
    $container = \Drupal::getContainer();
    $container->set('keyvalue.memory', new KeyValueMemoryFactory());
    $parameter = $container->getParameter('factory.keyvalue');
    $parameter['system.schema'] = 'keyvalue.memory';
    $container->set('keyvalue', new KeyValueFactory($container, $parameter));
    require_once \Drupal::root() . '/core/includes/update.inc';
    module_load_install($module);
    update_do_one($module, $number, [], $context);
  }

}
